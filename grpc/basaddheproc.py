# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Basic additive homomorphic encryption, as in basaddhe.py.
This implementation uses processes and IPC by protobuf/grpc.
The processes are:
- one for each meter producing a meter value and choosing a secret key,
- one for adding the secret keys, this controls a minimum number of meters to be involved.
- one for decrypting the sum by using the encrypted meter values and secret key sum.
- one as time controller, also starting/stopping the above processes.

Messages used:
From controller to each meter: start one cycle, set modulus, generate a secret key.
From controller to secret key adder: set modulus.
From each meter to secret key adder: secret key for meter.
From controller to each meter: set meter value.
From controller to sum decryptor: set modulus.
From each meter to sum decryptor: encrypted meter value.
From decryptor to secret key adder: request secret key sum for a set of meters.
From decryptor to controller: decrypted sum.
"""

import random
import grpc
from concurrent import futures # for threads.
import logging

from basaddhe_pb2_grpc import MeterStub, MeterServicer, add_MeterServicer_to_server
from basaddhe_pb2_grpc import SecretKeyAdderStub, SecretKeyAdderServicer
from basaddhe_pb2_grpc import SumDecryptorStub, SumDecryptorServicer
from basaddhe_pb2_grpc import ControllerStub, ControllerServicer

from basaddhe_pb2 import Modulus
from basaddhe_pb2 import MeterValue
from empty_pb2 import Empty

# Each *Stub defines methods
# Each *Servicer defines a constructor with a grpc Channel

# Use a thread for each *Servicer to implement the methods, follow the grpc helloworld.
# Use a socket for each Channel at each *Stub.


class Meter(MeterServicer):

  def StartCycle(self, request, context): # CHECKME: how to access the request?
      self.modulus = request.modulus
      print("Meter.StartCycle modulus " + str(modulus))
      return Empty()

  def SetMeterValue(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

# CHECKME: how to return a correct value from an rpc?


def startMeterServer():
    logging.basicConfig()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10)) # CHECKME: how to terminate these threads?
    add_MeterServicer_to_server(Meter(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    return server


if __name__ == '__main__':
    meterServer = startMeterServer()
    try:
        channel = grpc.insecure_channel('localhost:50051')
        meterStub = MeterStub(channel)

        modulus = 10000
        print("modulus = %i" % modulus)

        response = meterStub.StartCycle(Modulus(modulus=modulus))
        print("StartCycle response class: " + repr(response.__class__))
    finally:
        meterServer.stop(1)
        meterServer.wait_for_termination()


    num_addends = 30
    print("num_addends = %i" % num_addends)
    # random.seed(17)
    addend_encryptors = [AddendEncryptor(modulus) for i in range(num_addends)]
    sum_decryptor = SumDecryptor(modulus)

    # encryptors chose their keys.
    key_total = 0
    for addend_encryptor in addend_encryptors:
        key = random.randrange(modulus)
        addend_encryptor.set_key(key) # use random numbers as keys
        key_total += key
    sum_decryptor.set_key((- key_total) % modulus) # keys sum to zero.

    # set addends to random numbers for test.
    total_addends = 0
    for addend_encryptor in addend_encryptors:
        addend = random.randrange(modulus)//num_addends # avoid overflow above modulus
        total_addends += addend
        addend_encryptor.set_addend(addend)
    assert total_addends < modulus
    print("total_addends = %i" % total_addends)

    total = sum_decryptor.get_sum(addend_encryptors) # can use open communication
    print("decrypt total = %i" % total)

    assert total == total_addends


class AddKeyModulo(object):
    def __init__(self, modulus):
        assert isinstance(modulus, int)
        assert modulus > 1
        self.modulus = modulus

    def set_key(self, key):
        assert isinstance(key, int)
        self.key = key

    def add_key_modulo(self, value):
        assert isinstance(value, int)
        return (value + self.key) % self.modulus


class MeterValueProducer(AddKeyModulo):

    def setMeterValue(self, val):
        self.val = val

    def encrypt_addend(self):
        return self.add_key_modulo(self.val)


class SumDecryptor(AddKeyModulo):

    def get_sum(self, mvps): # can use open communication
        self.total = sum(mvp.encrypt_addend() for mvp in mvps)
        print("SumDecryptor.get_sum total %i" % self.total)
        return self.add_key_modulo(self.total)



if __name__ == "__main__":
    test_additive_encryption_processes()
