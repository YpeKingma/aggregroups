# Copyright 2020  Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest

from crt import CRT

class CRTtests(unittest.TestCase):

    def tstCRTb(self, crt, moduli, remainders, expected):
        res = crt.remainder(remainders)
        self.assertEqual(res, expected)
        for m, r in zip(moduli, remainders): # chinese remainder
            self.assertTrue(res % m == r)

    def tstCRT(self, moduli, remainders, expected):
        self.tstCRTb(CRT(moduli), moduli, remainders, expected)

    def testCRT1(self): self.tstCRT((3,5), (2,3), 8)

    def testCRT2(self): self.tstCRT((3,5,7), (2,3,2), 23)

    def testCRT3(self): self.tstCRT((3,5,7,11), (2,3,2,1), 23)

    def testCRT4(self): self.tstCRT((3,5,7,11), (2,3,2,2), 233)

    def tstCRT2remainders(self, moduli, remainders1, expected1, remainders2, expected2):
        crt1 = CRT(moduli)
        self.tstCRTb(crt1, moduli, remainders1, expected1)
        self.tstCRTb(crt1, moduli, remainders2, expected2)

    def testCRT2remainders(self):
        self.tstCRT2remainders((3,5,7,11), (2,3,2,1), 23, (2,3,2,2), 233)

    def tstCRTnoncoprime(self, moduli):
        with self.assertRaises(ValueError):
            CRT(moduli)

    def testCRTnoncoprime1(self): self.tstCRTnoncoprime((33,7,11))

    def testCRTnoncoprime2(self): self.tstCRTnoncoprime((33,5,7,11))


if __name__ == '__main__':
    unittest.main()
