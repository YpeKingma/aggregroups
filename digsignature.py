# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" A digital signature that is provably secure.
This is based on the following articles:

@article{bellare1998pss,
  title={PSS: Provably secure encoding method for digital signatures},
  author={Bellare, Mihir},
  year={1998},
  publisher={Citeseer}
}

@inproceedings{bellare1996exact,
  title={The exact security of digital signatures-How to sign with RSA and Rabin},
  author={Bellare, Mihir and Rogaway, Phillip},
  booktitle={International conference on the theory and applications of cryptographic techniques},
  pages={399--416},
  year={1996},
  organization={Springer}
}

For terminology see also RFC 4055, https://tools.ietf.org/html/rfc4055

This implementation uses SHA-256 as the compressor hash function.
The generator hash function g is implemented as ?
"""

import hashlib

from numberutil import randomIntBitSize

def _intToByteArray(m):
    assert m >= 0
    mBitSize = m.bit_length()
    mBytes = bytearray(min(1, (mBitSize + 7) // 8))
    i = 0
    while m > 0:
        m, mBytes[i] = divmod(m, 256)
        i += 1
    return mBytes

def _intFromByteArray(mBytes):
    m = 0
    for mb in mBytes:
        assert 0 <= mb < 256
        m = 256 * m + mb
    return m

def _bitMask(numBits):
    assert numBits >= 0
    return (2 ** numBits) - 1


class PSS(object):
    """ Probabilistically Secure Signature """
    # See bellare1996exact above
    def __init__(self, k0 = 256):
        """ k0, is the bit length of a random seed, typically 128 (as of 1996)
        """
        assert k0 >= 1
        assert k0 % 8 == 0 # allow byte wise shifts
        self.k0 = k0
        self.k1 = 256 # bit length of digest hashlib.sha256()

    def _g(self, m, k):
        """
        Return a hash of m consisting of k - k0 - 255 bits, as a bytearray.
        m consists of 256 bits.
        k >=  k0 + 255
        """
        # See bellare1996exact Appendix.
        numBitsForHash = k - self.k0 - self.k1 - 1
        assert numBitsForHash >= 0
        rBytes = bytearray((numBitsForHash + 7) // 8)
        rBi = 0 # index in rBytes
        i = 7
        cnst = 173
        mBytes = _intToByteArray(m)
        while numBitsForHash > 0:
            sha256 = hashlib.sha256()
            sha256.update(_intToByteArray(cnst + 256 * i))
            i += 1
            sha256.update(mBytes)
            hBytes = sha256.digest()

            numBytesForHash = min((numBitsForHash + 7) // 8, len(hBytes))
            assert numBytesForHash > 0
            hBi = 0
            while numBytesForHash > 0:
                rBytes[rBi] = hBytes[hBi]
                rBi += 1
                hBi += 1
                numBytesForHash -= 1
                numBitsForHash -= 8
        return rBytes

    def sign(self, message, rsaDecryptor, rnd):
        assert type(message) == bytearray
        k = rsaDecryptor.nBitSize()
        assert self.k0 + self.k1 < k

        r = randomIntBitSize(self.k0, rnd) # k0: bit length of the random seed.
        sha256 = hashlib.sha256()
        sha256.update(_intToByteArray(r))
        sha256.update(message)
        wBytes = sha256.digest()
        assert len(wBytes) = self.k1 // 8 # k1: bit length of the compressing hash
        w = _intFromByteArray(wBytes) # k0 bits
        gw = self._g(w, k) # a hash consisting of k - k0 - k1 - 1 bits
        g1w = w & _bitMask(self.k0) # k0 bits
        rStar = g1w ^ r # k0 bits
        maskg2 = _bitMask(k - self.k0 - self.k1 - 1)
        g2w = (gw >> self.k0) & maskg2
        y = (  g2w                                   # g2w   has k - self.k0 - self.k1 - 1 bits
            | (rStar << (k - self.k0 - self.k1 - 1)) # rStar has k0 bits
            | (w     << (k - self.k1 - 1))           # w     has k1 bits
            )
        # y has k-1 bits in order to guarantee that y < N, i.e. y is in Z*N (???)
        assert 0 <= y
        assert y < rsaDecryptor.n
        s = rsaDecryptor.decrypt(y)
        return s

    def verify(self, signature, rsaEncryptor):
        """ Verify signature (a bytearray) against the rsaEncryptor """
        y = rsaEncryptor.encrypt(signature)
        k = rsaEncryptor.nBitLength()
        gammaBits = k - self.k0 - self.k1 - 1
        gamma = y & _bitMask(gammaBits)
        y >>= gammaBits
        rStarBits = self.k0
        rStar = y & _bitMask(rStarBits)
        y >>= rStarBits
        wBits = self.k1
        w = y & _bitMask(wBits)
        assert y == w
        r = rStar ^ w

        sha256 = hashlib.sha256()
        sha256.update(_intToByteArray(r)) # FIXME: needs k0 bits.
        sha256.update(signature) # FIXME: needs k bits.
        wBytesFromSig = sha256.digest()

        wFromSig = _intFromByteArray(wBytesFromSig)

        if wFromSig != w:
            return False

        gw2FromSig = ...




