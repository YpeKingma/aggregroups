# -*- coding: utf-8 -*-

# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
See aggregroups.py for background.

Provide meter groups by using CRT.
Household meters are in pairs.
No failing household meters.
No separate gk for each group.
"""

from numberutil import nextPrime, secureRandom, pow_mod
from crt import CRT
from phe import generateKeyPairScheme1

from aggregroups import UtilityProvider, Aggregator, HouseholdMeter

from hashtime import HashTimeCoPrime
import datetime


class HouseholdMeterInPair(HouseholdMeter):
    def __init__(self, name, group, hashFunction):
        HouseholdMeter.__init__(self, name, group)
        self.hashFunction = hashFunction

    def initPair(self, otherHM):
        self.alpha = secureRandom.randrange(1, self.publicKey.n)
        otherHM.alpha = - self.alpha

    def setMeasurementTime(self, m, dt):
        self.setMeasurement(m)
        self.dt = dt

    def encryptMeasurement(self):
        cm = self.publicKey.encrypt(self.messageInGroup())
        ht = self.hashFunction(self.dt)
        hpna = pow_mod(ht, self.publicKey.n + self.alpha, self.publicKey.n2)
        return (cm * hpna) % self.publicKey.n2


def simulateSmallGroupsWithPairs():
    utpr = UtilityProvider()
    aggr = Aggregator()
    numGroups = 2

    utpr.createPaillierKeyPair()

    hm00 = HouseholdMeterInPair(name="hm00", group=0, hashFunction=HashTimeCoPrime(utpr.publicKey.n))
    hm01 = HouseholdMeterInPair(name="hm01", group=0, hashFunction=HashTimeCoPrime(utpr.publicKey.n))
    hm10 = HouseholdMeterInPair(name="hm10", group=1, hashFunction=HashTimeCoPrime(utpr.publicKey.n))
    hm11 = HouseholdMeterInPair(name="hm11", group=1, hashFunction=HashTimeCoPrime(utpr.publicKey.n))
    hMeters = (hm00, hm01, hm10, hm11)
    numHMeters = len(hMeters)

    # inform others of public key:
    aggr.setPublicKey(utpr.publicKey)
    for hm in hMeters:
        hm.setPublicKey(utpr.publicKey)

    maxMeasurement = 500000

    groupPrimes = utpr.createGroupPrimes(numHMeters=numHMeters, maxMeasurement=maxMeasurement, numGroups=numGroups)
    for hm in hMeters:
        hm.setGroupPrimes(groupPrimes)

    # create household meter pairs, and exchange secret key within each pair.
    for hma, hmb in ((hm00, hm01), (hm10, hm11)):
        hma.initPair(hmb)

    msmnts = (10,11,21,22)
    dt = datetime.datetime.now() # all measurements at the same time.
    for hm, ms in zip(hMeters, msmnts):
        hm.setMeasurementTime(ms, dt)

    # encrypt measurements and send to aggregator
    for hm in hMeters:
        aggr.receiveMeasurement(hm, hm.encryptMeasurement())

    # aggregate and send to utility provider
    utpr.receiveTotals(aggr.aggregateMeasurements())
    groupTotals = utpr.decryptGroupTotals()
    for gtAct, gtExp in zip(groupTotals, (msmnts[0]+msmnts[1], msmnts[2]+msmnts[3])):
        assert gtAct == gtExp


if __name__ == "__main__":
    simulateSmallGroupsWithPairs()

