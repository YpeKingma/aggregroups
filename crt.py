# Copyright 2020  Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Algorithm for Chinese Remainder Theorem.
See also https://en.wikipedia.org/wiki/Chinese_remainder_theorem .
"""

from functools import reduce
from operator import __add__, __mul__

from numberutil import multInv

class CRT(object):
    """ Chinese remainder algorithm for constant moduli. """
    def __init__(self, moduli):
        """ The moduli should have a value of at least 2 and be pairwise co-prime. """
        self.moduli = tuple(moduli)
        self.P = reduce(__mul__, self.moduli)
        dim = len(self.moduli)
        self.invpps = []
        for i in range(dim):
            md = self.moduli[i]
            assert md >= 2
            pp = (self.P // md)   if dim >= 4 else \
                 reduce(__mul__, [self.moduli[j] for j in range(dim) if not j == i])
            inv = multInv(pp, md)
            self.invpps.append(inv * pp)

    def __getitem__(self, key):
        """ Access the factors used for remainder computation by self[key] """
        return self.invpps.__getitem__(key)

    def moduli_product(self):
        """ Return the product of the moduli """
        return self.P

    def remainder(self, remainders):
        """ For a number n that has the given remainders when divided by the moduli
            in the same order,
            return the remainder of the division of n by the product of the moduli.
        """
        remainders = tuple(remainders) # generator len() is not defined.
        assert len(remainders) == len(self.moduli)
        s = reduce(__add__, ((rem * invpp) for rem, invpp in zip(remainders, self)))
        return s % self.P
