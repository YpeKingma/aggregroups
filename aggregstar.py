# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Simulation of the protocol proposed by by Rane, Freudiger, Brito and Uzun 2015:
@inproceedings{rane2015privacy,
  title={Privacy, efficiency \& fault tolerance in aggregate computations on massive star networks},
  author={Rane, Shantanu and Freudiger, Julien and Brito, Alejandro E and Uzun, Ersin},
  booktitle={2015 IEEE International Workshop on Information Forensics and Security (WIFS)},
  pages={1--6},
  year={2015},
  organization={IEEE}
}
Single cohort: see section III, protocol steps 1) to 6), and Fig. 1.
Hierarchy of cohorts: see section IV.
"""

from functools import reduce
from operator import __add__, __mul__

from numberutil import nextPrime, secureRandom
from sss import make_random_shares, recover_secret
from phe import generateKeyPairScheme1

import sss # Shamir secret sharing


class Aggregator(object):

    def createSecretSharingModuloPrime(self, maxMeasurement, numParticipants):
        maxTotalMeasurement = (maxMeasurement + 1) * numParticipants # m * dmax.
        # choose secret modulo primes just bigger than maxTotalMeasurement
        secretModuloPrime = nextPrime(maxTotalMeasurement + 1) # see Section III, protocol step 1
        self.beta = secretModuloPrime
        return secretModuloPrime

    def initEncryptedPoints(self):
        self.encryptedPointsByParticipant = {}

    def receiveEncryptedPoints(self, encryptedPoints, fromPi):
        self.encryptedPointsByParticipant[fromPi] = encryptedPoints

    def generateR(self):
        self.r = secureRandom.randrange(self.beta)

    def homomorphicAddPoints(self, participants):
        # Rane 2015, p. 3, protocol steps 4, 5 and 6
        participants = tuple(participants) # len() used
        assert len(participants) >= 2
        totalPoints = []
        for pj in participants:
            # step 4)
            cj = pj.v.encrypt(self.r)
            xiPrev = None
            for pi in participants:
                xi, cyi = self.encryptedPointsByParticipant[pi][pj]
                if xiPrev is not None:
                    assert xiPrev == xi # make sure to add the correct shares.
                xiPrev = xi
                cj = pj.v.homomorphic_add(cj, cyi)
            # step 5)
            yj = pj.decrypt(cj) # participant pj should allow the aggregator to let pj to a decryption.
            xj = xiPrev
            totalPoints.append((xj, yj))
        # step 6)
        totalMeasurement = (recover_secret(totalPoints, prime=self.beta) - self.r) % self.beta
        return totalMeasurement


class Participant(object): # one meter per participant
    def __init__(self, name):
        self.name = name

    def createPaillierKeyPair(self, nBitSize):
        self.nBitSize = nBitSize
        (self.v, self._secretKey) = generateKeyPairScheme1(nBitSize=self.nBitSize, rnd=secureRandom)
        # self.v is the public key

    def setSecretSharingModuloPrime(self, beta):
        self.beta = beta

    def setMeasurement(self, di):
        self.di = di

    def generateSecretSharingPolynomialPoints(self, k, m, rnd):
        self.points = sss.make_random_shares_for_secret(
                            minimum=k,
                            num_shares=m,
                            secret=self.di,
                            prime=self.beta,
                            rnd=rnd)
        assert recover_secret(self.points, prime=self.beta) == self.di # NOCOMMIT: debug check

    def encryptPointsForParticipants(self, participants):
        res = {}
        for pi, point in zip(participants, self.points):
            x, y = point
            res[pi] = (x, pi.v.encrypt(y))
        return res

    def decrypt(self, c):
        return self._secretKey.decrypt(c)



def simulateSingleCohort1(testCase): # See Rane 2015, p. 3, right column.
    assert testCase
    rnd = testCase.getRandom()

    aggr = Aggregator()
    p0 = Participant(name="p0")
    p1 = Participant(name="p1")
    p2 = Participant(name="p2")
    participants = (p0, p1, p2)
    m = len(participants)

    maxMeasurement = 500000
    dmax = maxMeasurement + 1 # measurement 0 < di < dmax

    nBitSize = 220

    for pi in participants: # each participant has a key pair for homomorphic encryption
        pi.createPaillierKeyPair(nBitSize=nBitSize)

    k = m - 1 # fault tolerance parameter CHECKME: k participants needed to determine secret?
    assert 0 < k < m

    # step 1)
    beta = aggr.createSecretSharingModuloPrime(maxMeasurement=maxMeasurement, numParticipants=m)
    # TBD: add check that nBitSize is big enough for beta; which check is that precisely?
    for pi in participants:
        pi.setSecretSharingModuloPrime(beta)

    msmnts = (10,11,12)
    assert len(msmnts) == len(participants)
    for pi, di in zip(participants, msmnts):
        pi.setMeasurement(di)

    # step 2) first part of step 3)
    for pi in participants:
        pi.generateSecretSharingPolynomialPoints(k, m, rnd=rnd)

    # rest of step 3): encrypt the points for all other participants and send to A
    aggr.initEncryptedPoints()
    for pi in participants:
        aggr.receiveEncryptedPoints(pi.encryptPointsForParticipants(participants), pi)

    # step 4, 5, 6
    aggr.generateR() # choose rj randomly, will be added into mi's, TBD: this may affect minimum key bit size.
    actTotal = aggr.homomorphicAddPoints(participants)

    expTotal = reduce(__add__, msmnts)
    testCase.assertEqual(actTotal, expTotal)

