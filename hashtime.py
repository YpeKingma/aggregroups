# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import platform
isJython = (platform.python_implementation() == "Jython")

import hashlib
from numberutil import gcd

from datetime import datetime
_epoch = datetime(1970, 1, 1) # ignore timezone

def hashTime(dt):
    """ Return a non negative 256 bit integer hash for a given datetime object, ignoring the subsecond time.
        The given datetime should be after 1969.
    """
    seconds = int((dt - _epoch).total_seconds())
    return hashNonNegNumber(seconds)


def hashNonNegNumber(n):
    """ Return a non negative 256 bit integer hash for a non negative number. """
    assert n >= 0
    m = hashlib.sha256()
    byteArray = bytearray(1)
    while True:
        n, byteArray[0] = divmod(n, 256)
        m.update(byteArray if not isJython else str(byteArray))
        if n == 0:
            break

    dg = m.digest()
    assert len(dg) == 32 # sha256

    isIntType = (type(dg[0]) == type(0))
    dn = 0
    for b in dg:
        if isIntType: # python 3
            bn = b if b >= 0 else b + 256
        else: # character
            bn = ord(b)
        # assert 0 <= bn < 256
        dn = (dn << 8) + bn
    return dn


class HashTimeCoPrime(object):
    """ Provide a hash h, 2 <= h < n, h coprime with n,
        when called with a datetime object dt after 1969 ignoring the subsecond time.
    """
    def __init__(self, n):
        assert n > 2
        self.n = n

    def __call__(self, dt):
        dn = hashTime(dt) % self.n # dn < n
        if dn < 2: # never coprime
            dn = 2 # n > 2, dn < n
        while gcd(dn, self.n) != 1: # dn < n-1 because gcd(n-1,n) == 1
            dn += 1
        return dn # coprime, 2 <= dn < n
