# -*- coding: utf-8 -*-

# Copyright 2020  Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Numerical utilities.
Also provide alternatives for gmpy2 when gmpy2 is not available.
"""

import random

try:
    import secrets
    secureRandom = secrets.SystemRandom()
except ImportError: # before python 3.6
    secureRandom = random.SystemRandom()

try:
    # raise ImportError # simulate gmpy2 not available
    from gmpy2 import mpz
    from gmpy2 import powmod

    pow_mod = powmod

    from gmpy2 import gcd

    from gmpy2 import is_prime

    def isProbablePrime(n, trials=25):
        return is_prime(n, trials)

    from gmpy2 import next_prime
    nextPrime = next_prime #  """ For given q, return the smallest p > q that is probable prime """

    from gmpy2 import lcm
    from gmpy2 import divm # divm(r, b, m) returns x such that b * x == r modulo m.

    def multInv(a, n):
        """ Return t such that (a * t) % n == 1 and 0 < t < n"""
        try:
            return divm(1, a, n)
        except ZeroDivisionError:
            raise ValueError("not invertible")

except ImportError: # gmpy2 not available

    def mpz(n): # to avoid duplicate code
        return n

    # pow_mod = pow # for profiling:
    def pow_mod(base, exp, mod):
        return pow(base, exp, mod)

    try:
        from math import gcd
    except ImportError: # before python 3.5
        from fractions import gcd
    except ImportError: # before python 2.6
        def gcd(a, b): # Warning: untested.
            """Calculate the Greatest Common Divisor of a and b.
            Unless b==0, the result will have the same sign as b (so that when
            b is divided by it, the result comes out positive).
            """
            while b:
                a, b = b, (a % b)
            return a

    _smallPrimesList = [
        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
        59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127,
        131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193,
        197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269,
        271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349,
        353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431,
        433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503,
        509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599,
        601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673,
        677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761,
        769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857,
        859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947,
        953, 967, 971, 977, 983, 991, 997]

    _smallPrimesDict = {}
    for p in _smallPrimesList:
        _smallPrimesDict[p] = 1

    def isProbablePrime(n, trials=8):
        """ Return value indicates that n is probably a prime.
        The probability that True is returned for a non prime n is less than 4**(-trials).
        See also https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test .
        """
        if n < 2:
            return False
        if n in _smallPrimesDict:
            return True
        for p in _smallPrimesList:
            if n % p == 0:
                return False
        nMinus1 = n - 1
        r = 0
        d = nMinus1
        while d % 2 == 0:
            d >>= 1
            r += 1

        def checkWitness(a): # for n composite
            x = pow_mod(a, d, n)
            if x == 1 or x == nMinus1:
                return False # continue witness loop
            for _ in range(r - 1):
                x = pow_mod(x, 2, n)
                if x == nMinus1:
                    return False # continue witness loop
            return True

        for _ in range(trials): # witness loop
            if checkWitness(random.randrange(2, nMinus1)):
                return False
        return True


    def nextPrime(q):
        """ Return the smallest p > q that is probable prime """
        p = q | 1
        if p == q:
            p += 2
        while True:
            if isProbablePrime(p):
                return p
            p += 2

    def lcm(a, b):
        return a * b // gcd(a, b)


    try:
        pow(3, -1, 5) # 2, negative power available in python 3.8

        def multInv(a, n):
            """ Return t such that (a * t) % n == 1 and 0 < t < n"""
            return pow(a, -1, n)

    except: # ValueError in python 3 before 3.8, TypeError in python 2 and jython.

        def multInv(a, n):
            """ Return t such that (a * t) % n == 1 and 0 < t < n"""
            # See also https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm .
            t, nt = 0, 1
            r, nr = n, a
            while nr != 0:
                q = r // nr
                t, nt = nt, t - q * nt
                r, nr = nr, r - q * nr
            if r > 1:
                raise ValueError("not invertable")
            if t < 0:
                t += n
            # assert (a * t) % n == 1
            return t

try:
    pow_mod(3, -1, 5) # 2, negative power available

    def pow_mod_all_exp(base, exp, mod):
        return pow_mod(base, exp, mod)

except: # ValueError or TypeError

    def pow_mod_all_exp(base, exp, mod):
        if exp < 0:
            base = multInv(base, mod)
            exp = - exp
        return pow_mod(base, exp, mod)



def randomIntBitSize(bitSize, rnd):
    p2 = 2 ** (bitSize - 1)
    res = rnd.randrange(p2, 2 * p2)
    assert res.bit_length() == bitSize
    return mpz(res)


def _generateCoPrime(n, nmax, rnd):
    """ Return a random number at least 2 that is coprime to n and smaller than nmax """
    assert n > 2
    assert nmax > 2
    maxR = min(n, nmax)
    rejections = 0
    while True:
        r = mpz(rnd.randrange(2, maxR))
        if gcd(r, n) == 1:
            return r
        rejections += 1
        assert rejections < 100 # avoid infinite loop, ease debugging.

def randomCoPrime(n, rnd):
    """ Return a random number at least 2 that is coprime to n and smaller than n. """
    return _generateCoPrime(n, n, rnd)

def generateRsaEncryptor(n, rnd):
    """ Return a number that is at least 2, smaller than n, coprime with n, and smaller than 2 ** 100 """
    # not too large to reduce the work for rsa encryption
    return _generateCoPrime(n, 2 ** 100, rnd)


def generateNPQ(nBitSize, rnd):
    """ Return (n, p, q) with with n == p * q
        The given nBitSize is bit size of n, at least 211.
        The given random number generator rnd is used help generate n, p and q.
        p and q will have about equal bit size, and will differ in their first 100 bits
        to avoid easy Fermat Factorization.
    """
    assert nBitSize >= 211 # Require p and q to be at least 105 bits, see Fermat factorization below.
    assert nBitSize <= 8192 # Depends on available processing speed.

    # Choose a random prime number p of nBitSize//2, and q of the remaining bitsize for nBitSize.

    # See also Rivest 1999: safe (Germain) primes do not hurt, but do not bring better protection
    # than large enough primes.
    # Strong primes (with a large factor in p+1) are not considered here,
    # see also https://gmpy2.readthedocs.io/en/latest/advmpz.html for corresponding checks.

    bitSizeP = nBitSize // 2

    while True:
        # to start looking for q after generating p; uniform distribution:
        nMin = randomIntBitSize(nBitSize, rnd)
        pMin = randomIntBitSize(bitSizeP, rnd)
        p = nextPrime(pMin)
        qMin = nMin // p # uses only the upper half of the bits of nMin;
        # the distribution of qMin is not uniform
        q = nextPrime(qMin)
        """
See https://en.wikipedia.org/wiki/Fermat%27s_factorization_method
and https://crypto.stackexchange.com/questions/5262/rsa-and-prime-difference
and https://crypto.stackexchange.com/questions/5698
            /ansi-x9-31-standards-for-generating-random-numbers
p and q should differ in their first 100 bits: abs(p-q) > 2^(nBitSize/2−100)
Here p.bit_length() = k//2
        """
        minBitLengthPQ = min(p.bit_length(), q.bit_length())
        assert minBitLengthPQ > 100 # avoid infinite loop
        if (abs(p-q) >> (minBitLengthPQ - 100)) == 0: # avoid easy Fermat factorization
            continue # retry
        n = p * q
        if n.bit_length() == nBitSize: # n can be too large, but quite unlikely.
            return (n, p, q)
