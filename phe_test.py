# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import randomseedtest

from numberutil import isProbablePrime
from crt import CRT
from phe import generateKeyPairScheme1, generateKeysScheme1FromNPQ


class PHEtests(randomseedtest.RandomSeedTestCase):

    def testProbablePrime(self):
        numBits = 500
        startRange = 2 ** numBits
        rangeSize = 10000
        numFound = 0
        for i in range(startRange, startRange + rangeSize):
            if isProbablePrime(i):
                numFound += 1
        self.assertEqual(numFound, 40)

    def tstEncryptDecrypt(self, pub, prv, mes):
        enc = pub.encrypt(mes)
        dec = prv.decrypt(enc)
        self.assertEqual(mes, dec)

    def tstPaillierKeySize(self, nBitSize, mes):
        rnd = self.getRandom()
        pub, prv = generateKeyPairScheme1(nBitSize=nBitSize, rnd=rnd)
        self.tstEncryptDecrypt(pub, prv, mes)

    def testPaillierKeySizes(self):
        mes = 301
        self.tstPaillierKeySize(211, mes)
        #self.tstPaillierKeySize(256, mes)
        #self.tstPaillierKeySize(512, mes)
        #self.tstPaillierKeySize(1024, mes)
        self.tstPaillierKeySize(2048, mes)
        #tstPaillierKeySize(4096, mes)
        #tstPaillierKeySize(8192, mes)

    def tstPaillierMessagesTwoPow(self, p, q):
        n = p * q
        rnd = self.getRandom()
        pub, prv = generateKeysScheme1FromNPQ(n, p, q, rnd)
        mes = 1
        while mes < pub.n:
            self.tstEncryptDecrypt(pub, prv, mes)
            mes <<= 1

    def testPaillierMessagesTwoPow(self):
        self.tstPaillierMessagesTwoPow(3,5)
        self.tstPaillierMessagesTwoPow(59,61)
        self.tstPaillierMessagesTwoPow(11,131)

    def testHomomorphic1AddProdPow(self):
        rnd = self.getRandom()
        pub, prv = generateKeyPairScheme1(nBitSize=2048, rnd=rnd)
        m1 = 301
        m2 = m1 + 987
        sum12 = (m1 + m2) % pub.n
        enc1 = pub.encrypt(m1)
        enc2 = pub.encrypt(m2)
        decSum = prv.decrypt(pub.homomorphic_add(enc1, enc2))
        self.assertEqual(decSum, sum12)
        prod = (m1 * m2) % pub.n
        decPow12 = prv.decrypt(pub.homomorphic_encrypt_mult(enc1, m2))
        self.assertEqual(decPow12, prod)
        decPow21 = prv.decrypt(pub.homomorphic_encrypt_mult(enc2, m1))
        self.assertEqual(decPow21, prod)
        decSum = prv.decrypt(pub.homomorphic_encrypt_add(enc1, m2))
        self.assertEqual(decSum, sum12)

    def test1Blinding(self):
        rnd = self.getRandom()
        pub, prv = generateKeyPairScheme1(nBitSize=2048, rnd=rnd)
        mes = 301
        r = rnd.randrange(2, pub.n)
        enc = pub.encrypt(mes % pub.n)
        self.assertEqual(prv.decrypt(enc), mes)
        blinded = pub.self_blind(enc, r)
        self.assertNotEqual(enc, blinded)
        decBlinded = prv.decrypt(blinded)
        self.assertEqual(decBlinded, mes)

    def tstEncryptAdd(self, pub, prv, a, b, s):
        enc1 = pub.encrypt(a)
        m2 = b
        decSum = prv.decrypt(pub.homomorphic_encrypt_add(enc1, m2))
        self.assertEqual(decSum, s)

    def tstPaillierModuloBorderCases(self, nBitSize):
        rnd = self.getRandom()
        pub, prv = generateKeyPairScheme1(nBitSize=nBitSize, rnd=rnd)
        self.tstEncryptDecrypt(pub, prv, 1)
        self.tstEncryptDecrypt(pub, prv, 2)
        self.tstEncryptDecrypt(pub, prv, pub.n - 2)
        self.tstEncryptDecrypt(pub, prv, pub.n - 1)
        self.tstEncryptAdd(pub, prv, 1, pub.n - 1, 0)
        self.tstEncryptAdd(pub, prv, pub.n - 1, 1, 0)
        self.tstEncryptAdd(pub, prv, pub.n - 1, 2, 1)
        self.tstEncryptAdd(pub, prv, 2, pub.n - 1, 1)

    def testPaillierModuloBorderCases(self):
        self.tstPaillierModuloBorderCases(212)
        self.tstPaillierModuloBorderCases(400)


if __name__ == '__main__':
    unittest.main()
