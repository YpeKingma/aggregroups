# -*- coding: utf-8 -*-

# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Simulation of the protocol proposed by Erkin 2015:
@inproceedings{erkin2015private,
  title={Private data aggregation with groups for smart grids in a dynamic setting using CRT},
  author={Erkin, Zekeriya},
  booktitle={2015 IEEE international workshop on information forensics and security (WIFS)},
  pages={1--6},
  year={2015},
  organization={IEEE}
}
The data aggregation protocol steps are in section IV A.

See Fig. 2.
The aggregator does not need the groupPrimes, even though they are available to everyone.

Provide meter groups by using CRT.
No household pairs, as if H(.) = 1
No failing household meters.
No separate gk for each group.
"""

from numberutil import nextPrime, secureRandom
from crt import CRT
from phe import generateKeyPairScheme1


class UtilityProvider(object):

    def createPaillierKeyPair(self):
        self.nBitSize = 220
        (self.publicKey, self._secretKey) = generateKeyPairScheme1(nBitSize=self.nBitSize, rnd=secureRandom)

    def createGroupPrimes(self, numHMeters, maxMeasurement, numGroups):
        maxGroupSize = numHMeters // numGroups
        if numHMeters > numGroups * maxGroupSize:
            maxGroupSize += 1
        maxGroupTotalMeasurement = maxMeasurement * maxGroupSize
        # choose group primes just bigger than maxGroupTotalMeasurement
        self.groupPrimes = []
        p = maxGroupTotalMeasurement
        while len(self.groupPrimes) < numGroups:
            p = nextPrime(p)
            self.groupPrimes.append(p)
        self.crt = CRT(self.groupPrimes)
        prodPrimes = self.crt.moduli_product()
        maxMessage = prodPrimes * maxGroupTotalMeasurement
        bitsNeeded = maxMessage.bit_length()
        assert bitsNeeded <= self.nBitSize
        return self.groupPrimes

    def receiveTotals(self, totalEnc):
        self.totalEnc = totalEnc

    def decryptGroupTotals(self):
        totalDecrypted = self._secretKey.decrypt(self.totalEnc)
        groupTotals = ((totalDecrypted % gp) for gp in self.groupPrimes)
        return groupTotals


class Aggregator(object):
    def __init__(self):
        self.measurements = {}

    def setPublicKey(self, publicKey):
        self.publicKey = publicKey

    def receiveMeasurement(self, hh, c):
        self.measurements[hh] = c

    def aggregateMeasurements(self):
        res = None
        for (hh, c) in self.measurements.items():
            res = c if res is None else self.publicKey.homomorphic_add(res, c)
        return res


class HouseholdMeter(object): # one meter per household
    def __init__(self, name, group):
        self.name = name
        self.group = group

    def setPublicKey(self, publicKey):
        self.publicKey = publicKey

    def setGroupPrimes(self, groupPrimes):
        self.crt = CRT(groupPrimes)

    def setMeasurement(self, m):
        self.m = m

    def messageInGroup(self):
        mg = (self.m * self.crt[self.group]) % self.crt.moduli_product()
        return mg

    def encryptMeasurement(self):
        return self.publicKey.encrypt(self.messageInGroup())


def simulateSmallGroups():
    utpr = UtilityProvider()
    aggr = Aggregator()
    numGroups = 2
    hm00 = HouseholdMeter(name="hm00", group=0)
    hm01 = HouseholdMeter(name="hm01", group=0)
    hm10 = HouseholdMeter(name="hm10", group=1)
    hm11 = HouseholdMeter(name="hm11", group=1)
    hMeters = (hm00, hm01, hm10, hm11)
    numHMeters = len(hMeters)

    utpr.createPaillierKeyPair()
    aggr.setPublicKey(utpr.publicKey)
    for hm in hMeters:
        hm.setPublicKey(utpr.publicKey)

    maxMeasurement = 500000

    groupPrimes = utpr.createGroupPrimes(numHMeters=numHMeters, maxMeasurement=maxMeasurement, numGroups=numGroups)
    for hm in hMeters:
        hm.setGroupPrimes(groupPrimes)

    msmnts = (10,11,21,22)
    for hm, ms in zip(hMeters, msmnts):
        hm.setMeasurement(ms)

    # encrypt measurements and send to aggregator
    for hm in hMeters:
        aggr.receiveMeasurement(hm, hm.encryptMeasurement())

    # aggregate and send to utility provider
    utpr.receiveTotals(aggr.aggregateMeasurements())
    groupTotals = utpr.decryptGroupTotals()
    for gtAct, gtExp in zip(groupTotals, (msmnts[0]+msmnts[1], msmnts[2]+msmnts[3])):
        assert gtAct == gtExp

if __name__ == "__main__":
    simulateSmallGroups()

