# -*- coding: utf-8 -*-

# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""  Homomorphic encryption as defined in this paper by Pascal Paillier (in bibtex format):
@inproceedings{paillier1999public,
  title={Public-key cryptosystems based on composite degree residuosity classes},
  author={Paillier, Pascal},
  booktitle={International Conference on the Theory and Applications of Cryptographic Techniques},
  pages={223--238},
  year={1999},
  organization={Springer}
}
Paillier 1999 is used to refer to this paper here.

The is an implementation of Scheme 1 of Paillier 1999.
The implemention was developed for use with python2 and python3.

Other references:

Koblitz:
Neal Koblitz, A course in number theory and cryptography, Springer, 1994, 2nd ed.

@inproceedings{rivest1999arestrong,
  title={AreStrong'Primes Needed for RSA?},
  author={Rivest, Ronald L and Silverman, Robert D},
  booktitle={IN THE 1997 RSA LABORATORIES SEMINAR SERIES, SEMINARS PROCEEDINGS},
  year={1999}
}
Referred to as Rivest 1999.

TBD: Implement the fast variant of Paillier, Scheme 3, and follow section 7
on efficiency and implementation aspects. Scheme 3 uses a shorter prime (160 bit) alpha,
1 <= alpha <= lmbda (Pailler, top of p. 232, end of section 6, p. 233).
See also the table on p. 235 for the expected speeds of the different schemes.
"""

from numberutil import pow_mod, lcm, gcd
from numberutil import randomCoPrime, randomIntBitSize
from numberutil import nextPrime, generateNPQ
from crt import CRT


class Scheme1PublicKey(object): # Paillier 1999, p. 229
    def __init__(self, n, n2, g, rnd):
        self.n = n
        self.n2 = n2
        self.g = g
        self.rnd = rnd # provide the random r for anonymity

    def _rnmodn2(self):
        # CHECKME: precompute this per encryption to increase encryption throughput.
        # See Paillier 1999, section 7, under Encryption, p. 233.
        r = randomCoPrime(self.n, self.rnd)
        return pow_mod(r, self.n, self.n2)

    def encrypt(self, m):
        """ Encrypt plaintext m using Scheme 1. """
        assert m >= 0 and m < self.n
        return (pow_mod(self.g, m, self.n2) * self._rnmodn2()) % self.n2

    def homomorphic_add(self, c1, c2):
        """ Homomorphic addition of two ciphertexts """
        assert c1 > 0 and c1 < self.n2
        assert c2 > 0 and c2 < self.n2
        return (c1 * c2) % self.n2

    def homomorphic_encrypt_mult(self, c, k):
        """ Encrypt k and multiply it into c. This uses the anonymity of c and is cheaper than encryption. """
        assert k > 0
        assert c > 0 and c < self.n2
        cpowk = pow_mod(c, k, self.n2)
        return cpowk % self.n2

    def homomorphic_encrypt_add(self, c, m):
        """ Encrypt m and add it into c. This uses the anonymity of c and is cheaper than encryption. """
        assert m >= 0 and m < self.n
        assert c > 0 and c < self.n2
        gpowm = pow_mod(self.g, m, self.n2)
        return (c * gpowm) % self.n2

    def self_blind(self, c, r):
        """ Change ciphertext c without affecting the message m, by multiplying with r ** n """
        assert r > 0
        assert c > 0 and c < self.n2
        rnmodn2 = pow_mod(r, self.n, self.n2)
        return (c * rnmodn2) % self.n2


def _L(u, n):
    # See Paillier 1999, p. 227, and https://en.wikipedia.org/wiki/Paillier_cryptosystem
    return (u - 1) // n # floor division

def _half_decrypt(c, p, pm1, pSquared, hp):
    # See Paillier 1999, p. 234, Decryption using Chinese-remaindering
    cpm1p2 = pow_mod(c, pm1, pSquared)
    lcp = _L(cpm1p2, p)
    return (lcp * hp) % p


class Scheme1PrivateKey(object): # Paillier 1999, p. 229
    def __init__(self, n, n2, g, p, q):
        self.n = n
        self.n2 = n2

        # Precomputations for using CRT for decryption, Paillier 1999, p. 234.
        self.p = p
        self.pm1 = self.p - 1
        self.pSquared = self.p ** 2
        gpm1p2 = pow_mod(g, self.pm1, self.pSquared)
        lgp = _L(gpm1p2, p)
        self.hp = pow_mod(lgp, self.pm1 - 1, p)
        assert (self.hp * lgp) % p == 1

        self.q = q
        self.qm1 = self.q - 1
        self.qSquared = self.q ** 2
        gqm1q2 = pow_mod(g, self.qm1, self.qSquared)
        lgq = _L(gqm1q2, q)
        self.hq = pow_mod(lgq, self.qm1 - 1, q)
        assert (self.hq * lgq) % self.q == 1

        self.crt = CRT((self.p, self.q))


    def decrypt(self, c):
        """ Decrypt an encrypted number, using Scheme 1. """
        assert c >= 0 and c < self.n2
        # Paillier 1999, p. 234, Decryption using Chinese-remaindering
        # to speed decryption up by splitting up over p and q:
        mp = _half_decrypt(c, self.p, self.pm1, self.pSquared, self.hp)
        mq = _half_decrypt(c, self.q, self.qm1, self.qSquared, self.hq)
        res = self.crt.remainder((mp, mq))
        return res


def generateKeyPairScheme1(nBitSize, rnd):
    """ Return a tuple of (Scheme1PublicKey, Scheme1PrivateKey) instances
        with n of the given bit size nBitSize.
        Random number generator rnd provides p, q, g and the r's.
    """
    (n, p, q) = generateNPQ(nBitSize, rnd)
    return generateKeysScheme1FromNPQ(n, p, q, rnd)


def generateKeysScheme1FromNPQ(n, p, q, rnd):
    assert p > 2
    assert q > 2 # both at least 2, otherwise there is no choice for g.
    n2 = n ** 2
    lmbda = lcm(p - 1, q - 1)
    g = generateG(n, n2, lmbda, rnd)
    publicKey = Scheme1PublicKey(n, n2, g, rnd)
    privateKey = Scheme1PrivateKey(n, n2, g, p, q)
    return (publicKey, privateKey)

def generateG(n, n2, lmbda, rnd):
    # Paillier uses g as an element of Z*n2 (see Paillier, p. 225,  under 3).
    # randomly select a base g from B by verifying eq (4) on p. 229:
    # gcd(L(g^lmbda mod n^2, n), n) = 1
    gRejections = 0
    while True:
        # TBD: Paillier 1999, p. 233 under Encryption, use a small g for encryption efficiency.
        g = randomCoPrime(n, rnd) # at least 2, element of Z*n2.
        if gcd(_L(pow_mod(g, lmbda, n2), n), n) == 1: # base g from B
            return g
        gRejections += 1
        assert gRejections < 100
