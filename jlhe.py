# -*- coding: utf-8 -*-

# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""  Homorphic encryption as defined in this paper by Joye and Libert (in bibtex format):
@inproceedings{joye2013scalable,
  title={A scalable scheme for privacy-preserving aggregation of time-series data},
  author={Joye, Marc and Libert, Beno{\^\i}t},
  booktitle={International Conference on Financial Cryptography and Data Security},
  pages={111--125},
  year={2013},
  organization={Springer}
}
JL 2013 is used to refer to this paper here.
Section 4 from JL 2013 is implemented.
"""
from __future__ import print_function

from functools import reduce
from operator import __add__

from numberutil import pow_mod_all_exp, gcd
from numberutil import isProbablePrime
from numberutil import generateNPQ
from hashtime import HashTimeCoPrime


def setUp(nBitSize, numEnc, rnd): # use nBitSize as the "input security parameter κ" of JL 2013.
    """ Return a tuple composition of (AggregDec, (Enc1, Enc2, ...)) instances
        with n of the given bit size nBitSize.
        Use rnd as random number generator to generate n = p * q,
        and to generate the secret keys.
        numEnc is the number of Enc instances.
    """
    # JL 2013: the trusted dealer randomly generates a modulus N = pq
    # which is the product of two equal-size primes p, q.
    (n, p, q) = generateNPQ(nBitSize, rnd)
    return setUpFromNPQ(n, p, q, numEnc, rnd)

def setUpFromNPQ(n, p, q, numEnc, rnd):
    assert isProbablePrime(p)
    assert isProbablePrime(q)
    # JL 2013: Note that size condition on p and q implies that gcd(φ(N), N) = 1
    assert abs(p.bit_length() - q.bit_length()) <= 1 # here slightly weaker than equal size
    phiN = (p-1)*(q-1)
    assert gcd(phiN, n) == 1 # CHECKME: gcd condition with small difference in p and q bit sizes?

    # Generate the secret keys
    maxS = 2 ** (2 * n.bit_length()) - 1
    minS = - maxS
    secretKeys = tuple(rnd.randrange(minS, maxS) for _ in range(numEnc))

    n2 = n ** 2
    H = HashTimeCoPrime(n2)
    encs = tuple(Enc(n, n2, H, sk) for sk in secretKeys)
    s0 = (- reduce(__add__, secretKeys)) # adapt when not all secret keys have been used for encryption.
    aggrDec = AggregDec(n, n2, H, s0)
    return (aggrDec, encs)


class Enc(object):
    def __init__(self, n, n2, H, sk):
        self.n = n
        self.n2 = n2
        self.H = H
        self.sk = sk

    def _htskmodn2(self, dt):
        # CHECKME: precompute this per encryption to increase encryption throughput.
        # See JL 2013, section 4, "exponentiations (H(t) ** sk) % n2 can be pre-computed"
        ht = self.H(dt)
        return pow_mod_all_exp(ht, self.sk, self.n2)

    def encrypt(self, m, dt):
        """ Encrypt plaintext m for the given time moment.  """
        assert m >= 0 and m < self.n
        return ((1 + m * self.n) * self._htskmodn2(dt)) % self.n2


class AggregDec(object):
    def __init__(self, n, n2, H, s0):
        self.n = n
        self.n2 = n2
        self.H = H
        self.s0 = s0

    def _hts0modn2(self, dt): # could be precomputed
        return pow_mod_all_exp(self.H(dt), self.s0, self.n2)

    def decrypt(self, cSequence, dt):
        """ Decrypt the sum of a sequence encrypted numbers for the given time moment. """
        vt = self._hts0modn2(dt)
        for c in cSequence:
            assert c >= 0 and c < self.n2
            vt = (vt * c) % self.n2
        xt = (vt - 1) // self.n
        return xt
