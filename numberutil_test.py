# Copyright 2020  Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest

from numberutil import lcm, gcd, pow_mod, nextPrime

class NumberUtilTest(unittest.TestCase):
    def testLcmGcdPow(self): # test some basic number properties, see Paillier 1999 Background
        p = nextPrime(17)
        q = nextPrime(p)
        n = p * q
        n2 = n * n # just over p**4
        lmbdaN = lcm(p-1, q-1)
        nLmbdaN = n * lmbdaN
        for w in range(2, n2):
            if gcd(w, n) == 1:
                self.assertEqual(pow_mod(w, lmbdaN, n), 1)
                self.assertEqual(pow_mod(w, nLmbdaN, n2), 1)
