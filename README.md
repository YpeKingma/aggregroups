# aggregroups

# Simulations of scenarios from the following papers:

"A scalable scheme for privacy-preserving aggregation of time-series data"
by Joye and Libert, 2013.
* See `jlhe.py` and `jlhe_test.py` for more details.


"Private data aggregation with groups for smart grids in a dynamic setting using CRT"
by Erkin, 2015.
* See `aggregroups.py`, `aggregroupspairs.py` and `aggregroups_test.py` for more details.


"Privacy, efficiency & fault tolerance in aggregate computations on massive star networks"
by Rane, Freudiger, Brito and Uzun, 2015.
* See `aggregstar.py` and `aggregstart_test.py` for more details.

## Homomorphic encryption

See:
* `phe.py` for an implementation of Paillier homomorphic encryption,
* `jlhe.py` for Joye Libert homomorphic encryption, and
* `sss.py` for Shamir secret sharing.

## Support code

See:
* `crt.py` for Chinese Remainder Theorem, and
* `numberutil.py` a.o. for primality testing.

# Licence

This is released under APL 2.0, see the LICENCE file, with the following
additional conditions:


For sss.ph:

Released into the Public Domain under the terms of the CC0 1.0 and the OWFa 1.0:
https://creativecommons.org/publicdomain/zero/1.0/
http://www.openwebfoundation.org/legal/the-owf-1-0-agreements/owfa-1-0


# Running the tests on Linux

Clone this repository, and have python3, python2 and jython installed.
In case some of these pythons are not available, adapt this line in `runtests.sh`:

    declare -a pythons=("python3" "python2" "jython")

Then, in the directory with the cloned repository run the following command:

    ./runtests.sh

The tests can also be run from a command line, for example:

    python -m unittest discover -p '*_test.py'

In case running the tests shows warnings for existing files
named `.randomseedfailed` and/or `.randomseed`,
please consult `randomseedtest.py` on how to proceed with testing.


To improve the runtimes install `gmpy2` for python.

See `numberutil.py` on how `gmpy2` is replaced when it is not available.


For profiling, see the commented lines in `runtests.sh` that contain cProfile and profile.
