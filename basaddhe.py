# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Basic additive homomorphic encryption, as defined in section 4.2 of
@article{castelluccia2009efficient,
  title={Efficient and provably secure aggregation of encrypted data in wireless sensor networks},
  author={Castelluccia, C and Chan, AC-F and Mykletun, E and Tsudik, G},
  journal={ACM Transactions on Sensor Networks},
  volume={5},
  number={3},
  pages={1--36},
  year={2009}
}

Encrypt all addends by adding a random number before sending them to be added,
and compute their (modulo) sum by subtracting the sum of the random numbers
used to encrypt the addends.
Use modulo computations to hide any trends in the addends.
"""

import random

class AddKeyModulo(object):
    def __init__(self, modulus):
        assert isinstance(modulus, int)
        assert modulus > 1
        self.modulus = modulus

    def set_key(self, key):
        assert isinstance(key, int)
        self.key = key

    def add_key_modulo(self, value):
        assert isinstance(value, int)
        return (value + self.key) % self.modulus


class AddendEncryptor(AddKeyModulo):

    def set_addend(self, addend):
        self.addend = addend

    def encrypt_addend(self):
        return self.add_key_modulo(self.addend)


class SumDecryptor(AddKeyModulo):

    def get_sum(self, addend_encryptors): # can use open communication
        self.total = 0
        for addend_encryptor in addend_encryptors:
            self.total += addend_encryptor.encrypt_addend()
        print("SumDecryptor.get_sum total %i" % self.total)
        return self.add_key_modulo(self.total)


def test_addend_encryption():
    num_addends = 30
    print("num_addends = %i" % num_addends)
    modulus = 10000
    print("modulus = %i" % modulus)
    # random.seed(17)
    addend_encryptors = [AddendEncryptor(modulus) for i in range(num_addends)]
    sum_decryptor = SumDecryptor(modulus)

    # encryptors chose their keys.
    key_total = 0
    for addend_encryptor in addend_encryptors:
        key = random.randrange(modulus)
        addend_encryptor.set_key(key) # use random numbers as keys
        key_total += key
    sum_decryptor.set_key((- key_total) % modulus) # keys sum to zero.

    # set addends to random numbers for test.
    total_addends = 0
    for addend_encryptor in addend_encryptors:
        addend = random.randrange(modulus)//num_addends # avoid overflow above modulus
        total_addends += addend
        addend_encryptor.set_addend(addend)
    assert total_addends < modulus
    print("total_addends = %i" % total_addends)

    total = sum_decryptor.get_sum(addend_encryptors) # can use open communication
    print("decrypt total = %i" % total)

    assert total == total_addends

if __name__ == "__main__":
    test_addend_encryption()
