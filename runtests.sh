#! /bin/bash -ef

# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

declare -a pythons=("python3" "python2" "jython")

FILE=.randomseed
if test -f "$FILE"; then
    echo "$0 Warning: $FILE exist, see randomseedtest.py"
fi

for python in "${pythons[@]}"; do
  cmd="$python -B -m unittest discover -p *_test.py" # bash -f option: no globbing here
  echo $cmd
  $cmd
done

# python2 -m cProfile -s cumtime phe_prof.py | (head -30; cat > /dev/null)
# python3 -m cProfile -s cumtime phe_prof.py | (head -30; cat > /dev/null)
# jython -m profile -s cumtime phe_prof.py | (head -30; cat > /dev/null)
# and similarly for jhle_prof.py

FILE=.randomseedfailed
if test -f "$FILE"; then
    echo "$0 Warning: $FILE exists, see randomseedtest.py"
fi

