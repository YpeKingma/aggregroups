# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from functools import reduce
from operator import __add__

import datetime
import unittest, randomseedtest

from jlhe import setUp, setUpFromNPQ
from numberutil import lcm, gcd

class JLheTest(randomseedtest.RandomSeedTestCase):
    def testSmall(self):
        p = 3
        q = 5
        ms = (3,8,2)
        numEnc = len(ms)
        (aggr, encs) = setUpFromNPQ(n=p*q, p=p, q=q, numEnc=numEnc, rnd=self.getRandom())
        dt = datetime.datetime(2019,11,26) + datetime.timedelta(seconds= 900 * self.getRandom().randrange(10000))
        cSeq = tuple(enc.encrypt(m, dt) for m, enc in zip(ms, encs))
        self.assertEqual(aggr.decrypt(cSeq, dt), reduce(__add__, ms))

    def testNormal(self):
        nBitSize = 2048
        numEnc = 25
        maxMeas = (2 ** 2047) // numEnc
        ms = tuple(self.getRandom().randrange(maxMeas) for _ in range(numEnc))
        (aggr, encs) = setUp(nBitSize=nBitSize, numEnc=numEnc, rnd=self.getRandom())
        # datetime: random quarter between end of 2010 and about 2040:
        dt = datetime.datetime(2010,11,26) + datetime.timedelta(seconds= 900 * self.getRandom().randrange(1000000))
        cSeq = tuple(enc.encrypt(m, dt) for m, enc in zip(ms, encs))
        self.assertEqual(aggr.decrypt(cSeq, dt), reduce(__add__, ms))


if __name__ == '__main__':
    unittest.main()
