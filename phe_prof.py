# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" Profile phe.py """
from numberutil import secureRandom
from phe import generateKeyPairScheme1

def testAdditionPerf(m, nBitSize):
    print("phe-testAdditionPerf", m, nBitSize)
    # Compute sum(1..m) via encryption and check the decryption for correctness.
    total = (1 + m) * m // 2 # see https://en.wikipedia.org/wiki/Triangular_number
    print("total", total)
    pub, prv = generateKeyPairScheme1(nBitSize=nBitSize, rnd=secureRandom)
    fac = (pub.n - 1) // total # get many non zero bits to be encrypted
    #print("fac", fac)
    #print("total * fac", total * fac)
    totalEnc = 1
    for i in range(1,m+1):
        enc = pub.encrypt(i * fac) # runtime is dominated by pow_mod call in encrypt()
        totalEnc = pub.homomorphic_add(totalEnc, enc)
        decTotal = prv.decrypt(totalEnc) # move outside loop to test encryption only.
        subtotal = (1 + i) * i // 2
        assert decTotal == subtotal * fac
    #print("decTotal", decTotal)
    assert decTotal == (total * fac)
    print("phe-testAdditionPerf passed")

# import cProfile
# cProfile.run('testAdditionPerf(m=100000, nBitSize=1024)')
# or run from a command shell by:
# python3 -m cProfile -s cumtime phe_prof.py | (head -30; cat > /dev/null)

testAdditionPerf(m=100, nBitSize=2048)
