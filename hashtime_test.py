# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import randomseedtest

from hashtime import HashTimeCoPrime
from numberutil import randomIntBitSize
import datetime

class HashTimeTest(randomseedtest.RandomSeedTestCase):

    def tstHTC(self, nBitSize):
        assert nBitSize >= 3
        n = randomIntBitSize(nBitSize, self.getRandom())
        H = HashTimeCoPrime(n)
        dt = datetime.datetime.now()
        h = H(dt)
        self.assertTrue(2 <= h < n)
        h2 = H(dt)
        self.assertEqual(h, h2)
        h3 = H(dt + datetime.timedelta(seconds=1))
        # assert h != h3 # not guaranteed ...          
        h2 = H(dt)
        self.assertEqual(h, h2)

    def testBitSizesHTC(self):
        for nBitSize in range(20):
            self.tstHTC(4 + nBitSize * 7)

if __name__ == '__main__':
    unittest.main()
