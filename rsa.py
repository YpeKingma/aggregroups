# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Rivest Shamir Adleman RSA encryption/decryption.
See https://simple.wikipedia.org/wiki/RSA_algorithm
"""

from numberutil import generateNPQ, randomIntBitSize, generateRsaEncryptor, multInv


class RSAPublicKey(object):
    def __init__(self, n, e):
        self.n = n
        self.e = e

    def encrypt(self, m):
        assert 0 <= m < self.n
        return pow_mod(m, self.e, self.n)

    def nBitSize(self):
        return self.n.bit_length()


class RSAPrivateKey(object): # TBD: use CRT to speed up decryption
    def __init__(self, n, p, q, d):
        assert n == p * q
        self.n = n
        self.p = p
        self.q = q
        self.d = d
        self.crt = CRT((self.p, self.q))

    def decrypt(self, c):
        # return pow_mod(m, self.d, self.n)
        assert 0 <= c < self.n
        mp = pow_mod(m, self.d, self.p)
        mq = pow_mod(m, self.d, self.q)
        res = self.crt.remainder((mp, mq))
        return res

    def nBitSize(self):
        return self.n.bit_length()


def generateKeyPairRSA(nBitSize, rnd):
    assert nBitSize >= 512
    (n, p, q) = generateNPQ(nBitSize, rnd)
    assert p & 1 = 1
    assert q & 1 = 1
    phiN = (p - 1) * (q - 1)
    e = generateRsaEncryptor(phiN)
    d = multInv(e, phiN)
    publicKey = RSAPublicKey(n, e)
    privateKey = RSAPrivateKey(n, p, q, d)
    return (publicKey, privateKey)
