# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
RandomSeedTestCase runs with a random number seed, or saves a random number seed when a test fails.

When a test seed is found in a file .randomseed, it is used instead of a SystemRandom, and it is not saved
even when the test fails.
When test a with random seed fails, the random seed is reported, and saved in another file .randomseedfailed .

To rerun a test that failed with a random seed copy the failing seed to be used as seed in the next test:

    cp .randomseedfailed .randomseed

and then rerun the test.
When the test passes and SystemRandom seeds can be used again, remove the file .randomseed.
The file .randomseedfailed can also be removed when it is no more needed.
"""

from __future__ import print_function

import unittest
import random
import os

_seedFile = ".randomseed"
_seedFileLastFail = ".randomseedfailed"


class RandomSeedTestCase(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        if os.path.isfile(_seedFile):
            f = open(_seedFile)
            seedString = f.read().strip()
            f.close()
            seed = int(seedString)
            print("Starting test case", self.id(), "with seed", seed, "from", _seedFile)
            self._randomSeedFromFile = True
        else:
            seed = random.SystemRandom().randrange(2 ** 64)
            self._randomSeedFromFile = False
        self._seed = seed
        self.random = random.Random(seed)

    def getRandom(self):
        """ The single random number generator for this test.
            This is initialized by the content from file .randomseed when available,
            otherwise it is initialized from a value provided by SystemRandom().
        """
        return self.random

    def run(self, testResult):
        self._testResult = testResult
        res = unittest.TestCase.run(self, self._testResult)
        if res != None: # from python 3.3
            self._testResult = res

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        if self._randomSeedFromFile: # do not overwrite _seedFileLastFail
            return
        if hasattr(self, "_outcome"): # python 3: use self._outcome instead of self._testResult
            testFailed = not self._outcome.success
            for testCase, excInfo in self._outcome.errors:
                testFailed |= excInfo is not None
        else:
            testFailed = self._testResult.errors or self._testResult.failures
        if testFailed:
            f = open(_seedFileLastFail, "w") # save seed in _seedFileLastFail
            f.write(str(self._seed))
            f.close()
            print("\nTest case", self.id(), "failed, saved seed", self._seed, "in", _seedFileLastFail)
            if hasattr(self, "stop"): # missing in jython
                self.stop() # run no more test cases in this suite.


class TRSTC(RandomSeedTestCase):

    def test1(self): # deliberately fail; module name does not end in _test.
        print(self.id())
        assert 1 == 2

    def testRandomSequence(self):
        random = self.getRandom()
        print("testRandomSequence", random.randrange(2 ** 64))
        print("testRandomSequence", random.randrange(2 ** 64))


if __name__ == "__main__":
    unittest.main()