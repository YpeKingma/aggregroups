# Copyright 2020 Ype Kingma
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import randomseedtest

from sss import make_random_shares_for_secret, recover_secret

class SecretShareTest(randomseedtest.RandomSeedTestCase):

    def testSecretOffset(self):
        secret = 10
        prime = 257
        offset = 250
        num_shares = 3
        minimum = 2
        points = make_random_shares_for_secret(
                        secret=secret,
                        minimum=minimum,
                        num_shares=num_shares,
                        prime=prime,
                        rnd=self.getRandom())
        offsetPoints = [(x, (y + offset) % prime) for x, y  in points]

        for i in range(num_shares):
            recoveredSecret = recover_secret(shares=points[0:i] + points[i+1:], prime=prime)
            self.assertEqual(recoveredSecret, secret)
            recoveredOffsetSecret = recover_secret(shares=offsetPoints[0:i] + offsetPoints[i+1:], prime=prime)
            self.assertEqual(recoveredOffsetSecret, (secret + offset) % prime) # linear: homomorphic

if __name__ == '__main__':
    unittest.main()
